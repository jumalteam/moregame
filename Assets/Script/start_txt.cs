﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class start_txt : MonoBehaviour {

    int isVisible;

    // Use this for initialization
    void Start()
    {
        isVisible = 0;
        StartCoroutine("fadeInOut", 1);
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator fadeInOut()
    {
        if (isVisible == 1)
        {
            isVisible = 0;
        }
        else
        {
            isVisible = 1;
        }

        yield return 1.0f;
    }
}
